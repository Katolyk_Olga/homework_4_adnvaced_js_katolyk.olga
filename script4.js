// Теоретичне питання
// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
// AJAX — це технологія, що дозволяє здійснювати асинхронний обмін даними між клієнтом та сервером без потреби перезавантажувати сторінку.
// Вебсторінка, не перезавантажуючись, у фоновому режимі надсилає запити на сервер і сама звідти довантажує потрібні користувачу дані.
// Після отримання відповіді, JavaScript виконує певні дії з цими даними, наприклад, оновлює вміст сторінки, 
// додає нову інформацію або змінює вигляд елементів сторінки.

// AJAX широко використовується в веб-розробці для різних завдань:
// - Оновлення динамічного вмісту: AJAX дозволяє оновлювати лише певні частини сторінки без перезавантаження всього вмісту 
// (наприклад коментарі в месенджерах або стрічка новин в соціальних мережах).
// - Відправлення та отримання даних форм: За допомогою AJAX можна відправляти дані з форм, такі як авторизація, повідомлення.
// - Асинхронна перевірка чи валідація даних: AJAX дозволяє виконувати перевірку даних на сервері асинхронно 
// (наприклад, перевіряти нові повідомлення на сторінці без перезавантажування).

// Завдання
// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

// Технічні вимоги:
// + 1. Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// + 2. Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. 
// Список персонажів можна отримати з властивості characters.
// + 3. Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. 
// + 4. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// + 5. Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.
// Необов'язкове завдання підвищеної складності
// + 6. Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. 
// + 7. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.

// Функція для запиту до сервера:
function sendRequest(type, url) {
    return new Promise ((resolve, reject) => {
        const request = new XMLHttpRequest();
        request.open(type, url);
        request.responseType="json";
        request.send();
        request.onload = function() {
            // console.log(request);  // можна подивитися прототип XMLHttpRequest, де є status, response та інші властивості
            if(request.status <= 300) {
                resolve(request.response)
                // console.log(request.response) // наш неопрацьований масив із Promise
            }else{
                reject(request.statusText)
            }
        }
    })
}

// Функція для виведення епізодів на екран:
function displayEpizodes(arrayEpizodes) {
    const container = document.createElement("ul");
    container.classList.add("epizodesContainer");
    container.innerHTML = `<h2>"These are episodes of "Star Wars":</h2>`;

    arrayEpizodes.forEach((objectEpizode) => {        
        const {episodeId, name, openingCrawl} = objectEpizode; // деконструкція потрібних даних з об'єкта епізода

        const epizode = document.createElement("li"); // тут ми створили окремий DOM-елемент для кожного епізоду
        epizode.classList.add("epizode");
        epizode.innerHTML =  `
            <h3>Epizode ${episodeId}: ${name}</h3>
            <p>${openingCrawl}</p>
        `;  
        container.appendChild(epizode); 

        // Виведення персонажів для кожного фільму під час перебору масиву епізодів, одразу, як епізоди виведуться на екран
        displayСharacters(objectEpizode, epizode);
    });
   
    document.body.append(container);
} 

// Функція для виведення персонажів для кожного епізоду на екран:
function displayСharacters(objectEpizode, epizode) {  // приймає об'єкт фільму з серверу та його DOM-елемент (li епізод, що створюється в функції displayEpizodes)
    const charactersContainer = document.createElement("ul");
    charactersContainer.classList.add("characters");
    charactersContainer.innerHTML = "<h4>These are the characters of this episode:</h4>"

    // Додаємо loader поруч з контейнером персонажів
    const loader = document.createElement("div");
    loader.classList.add("loader");    

    objectEpizode.characters.forEach((characterUrl, index) => {
        setTimeout(() => { // Імітація отримання персонажів з затримкою з отработкою loader
            sendRequest("GET", characterUrl)
            .then(character => {
                // console.log(character); // роздивляємось об'єкт персонажа, щоб знайти його ім'я
                const characterItem = document.createElement("li");
                characterItem.textContent = character.name;
                charactersContainer.appendChild(characterItem);
                
                // Перевірка на останнього персонажа
                if (index === objectEpizode.characters.length - 1) {
                    loader.style.display = "none"; // приховуємо loader після завантаження всіх персонажів
                }
            })
            .catch(error => {
                console.error("Помилка при отриманні інформації про персонажів:", error);
            });
        }, index * 1000);        
    });

    // epizode.appendChild(loader);
    epizode.appendChild(charactersContainer); // Додаємо контейнер персонажів до епізоду
    epizode.appendChild(loader); // наприкінці ставимо лоадер
}

// Робимо запит на сервер:
const seriesList = sendRequest ("GET", "https://ajax.test-danit.com/api/swapi/films");
console.log(seriesList); // отримали Promise зі статусом "fulfilled" і з вкладеним масивом зі всіма нашими данними

seriesList // обробляємо обіцянку Promise
.then( response => {
    // return response; // отримали наш масив з Promise
    const seriesList = response;    
    console.log("Це масив серій 'Зоряні війни':", seriesList);
    return seriesList
})
.then(seriesList => {
    displayEpizodes(seriesList);
    // return seriesList; // можно не повертати, тому що далі не обробляємо
})
.catch((error) => {console.log(error)});















// // Приклад 1 - простий fetch(): 
// const request = fetch("https://ajax.test-danit.com/api/swapi/films");
// request
//     .then(response => {
//         console.log(response); // отримали об'єкт-відповідь з описом (статус, body, headers, тощо...)
//         return response.json();
//     })
//    .then(result => console.log(result)); // отримали наш масив із відповіді




// // Приклад 2 - async з await fetch(): 
// const sendRequest = async(url, method="GET", config) => {  // можна тільки url, тоді GET за замовчуванням
//     const response = await fetch(url, {  // для методу GET другий параметр-об'єкт не потрібен
//         method: method, 
//         ...config
//     });
//     const result = await response.json(); ///перевели data в формае джайсон, тут await не обов'язковий	
//     return result;
// }

// const townsUrl = "https://ajax.test-danit.com/api/swapi/films";

// const response = sendRequest(townsUrl);

// response // обробляємо обіцянку Promise
//     .then(
//         result => console.log(result)
//     )
//     .catch((error) => {
//         console.log(error)
//     });



// // Приклад 3 короткий
// const sendRequest = async( url ) => { 
//     const response = (await fetch(url)).json();   
//     return response;
// }